import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  @Input() theme: string | undefined


  scrollToBottom() {
    window.scrollTo(0, document.body.scrollHeight);

  }

  ngOnInit(): void {
    
  }
}
