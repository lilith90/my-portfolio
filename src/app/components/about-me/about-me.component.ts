import { Component, HostListener, OnInit } from '@angular/core';
import { AboutMe } from 'src/app/services/about-me.service';

@Component({
  selector: 'app-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss']
})
export class AboutMeComponent implements OnInit {

  constructor(private aboutMeService: AboutMe) { }

  classArray = new Array(7).fill('')
  num: number = 0
  scrollY: number = 0
  currentScrollY = 330

  @HostListener('window:scroll', ['$event'])

  onScroll() {
    setTimeout(() => {
      if (window.scrollY > this.currentScrollY + this.scrollY) {
        this.classArray[this.num] = 'light'
        this.num++
        this.scrollY += 350      
      }
    }, 1500)
  }

  img = this.aboutMeService.img;
  description = this.aboutMeService.description;

  ngOnInit(): void {
    

  }

}
