import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @Input() theme: string | undefined
  arrow: any;

  scrollTop() {
    window.scrollTo({top: 0})
  }

  ngOnInit(): void {
    
      window.addEventListener("scroll", () => {
        this.arrow = document.querySelector('.arrow-box')
        if(window.pageYOffset > 150) {
          this.arrow.style.opacity = 1
        } else {
          this.arrow.style.opacity = 0
        }
      })
  }

}
